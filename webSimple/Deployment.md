# Prerequisite

- Docker 20 and above
- docker-compose 1.29 and above

# How to deploy

To start the application, simply run the following command:

`docker-compose up -d`

To stop the application, run:

`docker-compose down`