# TP 4 - Création de stacks web

![screenshot](https://gitlab.com/Madriax1/tp-stack-web/-/raw/main/doc/screenshot.png)

### Présentation:
>Dans ce repository, vous trouvez trois dossiers comprenant chacuns une application web simple réalisée avec une stack différente. Ces trois applications on pour seule fonctionnalité d'afficher le message “Bonjour ! Nous sommes le jj/mm/aaaa et il est hh:ii:ss” où  jj/mm/aaaa correspondra à la date courante et hh:ii:ss à l’heure courante.

## Outils utilisés
| Outil | Utilisation |
|-------|------|
|VSCode |IDE|
|Docker|Déploiement|
|Gitlab|Gestion des sources|

## Stacks

- [Simple web stack](https://gitlab.com/Madriax1/tp-stack-web/-/tree/main/webSimple)
- [PHP web stack](https://gitlab.com/Madriax1/tp-stack-web/-/tree/main/webPHP)
- [VueJS web stack](https://gitlab.com/Madriax1/tp-stack-web/-/tree/main/webVue)