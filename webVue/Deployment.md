# Prerequisite

- Docker 20 and above

# How to deploy

First, you need to build the application. Use the following command:

`docker build -t vue-app .`

Then, you can start the application by running:

`docker run -it -p 80:80 -d --name webVue vue-ap`

You can then stop the application like this:

`docker stop webVue`