const dateEl = document.querySelector('#date');
const hourEl = document.querySelector('#hour');

function leadingZeros(value) {
    return (`0${value}`).slice(-2);
}

function update() {
    const date = new Date();
    dateEl.innerHTML = `${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`;
    hourEl.innerHTML = `${leadingZeros(date.getHours())}:${leadingZeros(date.getMinutes())}:${leadingZeros(date.getSeconds())}`;
}

window.onload = () => {
    update();
    setInterval(update, 1000);
}